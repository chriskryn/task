//
//  AppError.swift
//  TakeAway
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import Foundation

public enum AppError: Error {
    case networkError
    case custom(String)
    case unknown
}

extension AppError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .networkError:
            return NSLocalizedString("Network Error", comment: "Invalid State")
        case .unknown:
            return NSLocalizedString("Something went wrong", comment: "Something went wrong")
        case .custom(let msg):
            return msg
        }
    }
}
