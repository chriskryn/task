//
//  PostCell.swift
//  TakeAway
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import UIKit

class PostCell: UICollectionViewCell {
    static let kReuseIdentifer = "kPostCell"
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!

    func configurePostCell( post: Post) {
        self.titleLabel.text = post.title
        self.authorLabel.text = post.author
        self.bodyLabel.text = post.body
    }
}
