//
//  PostsResponse.swift
//  TakeAway
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import Foundation
struct PostsResponse: Codable {
    var paging: Paging
    var posts: [Post]
}
