//
//  Paging.swift
//  TakeAway
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import Foundation
struct Paging: Codable {
    var nextCursor: String
    
    enum CodingKeys: String, CodingKey {
    case nextCursor = "next_cursor"
    }
}
