//
//  Post.swift
//  TakeAway
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import Foundation

struct Post: Hashable, Codable {
    let uid: String
    let author: String
    let title: String
    let body: String
}
