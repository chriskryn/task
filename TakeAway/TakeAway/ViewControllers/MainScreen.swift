//
//  ViewController.swift
//  TakeAway
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import UIKit

struct IntItem: Hashable {
    let id = UUID().uuidString
    var value: Int
}

typealias DataSource = UICollectionViewDiffableDataSource<IntItem, Post>

class MainScreen: UIViewController, UICollectionViewDelegate {
    private let service = NetworkService(username: "lalalal", password: "password")
    var data: [Post] = []
    var cursor: String?
    var dataSource: DataSource?
    var isLoading = false

    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "News Feed"
        self.collectionView.collectionViewLayout = self.createLayout()
        self.configureHierarchy()
        self.configureDataSource()
    }
    private func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(300.0)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .estimated(1000))
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitems: [item]
        )

        let section = NSCollectionLayoutSection(group: group)

        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }

    private func configureHierarchy() {
        self.collectionView.backgroundColor = .systemBackground
        self.collectionView.delegate = self
    }

    private func configureDataSource() {
        self.dataSource = UICollectionViewDiffableDataSource<IntItem, Post>(collectionView: self.collectionView) {
            (collectionView: UICollectionView, indexPath: IndexPath, item: Post) -> UICollectionViewCell? in

            if indexPath.row < self.data.count {
                guard let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: PostCell.kReuseIdentifer,
                    for: indexPath
                ) as? PostCell else {
                    fatalError("Cannot create new cell")
                }
                let post = self.data[indexPath.row]
                cell.configurePostCell(post: post)
                return cell
            } else {
                self.loadPosts()
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: "kLoading",
                    for: indexPath
                )
                return cell
            }
        }

        // initial data
        var snapshot = NSDiffableDataSourceSnapshot<IntItem, Post>()
        snapshot.appendSections([IntItem(value: 0)])
        var posts = self.data
        if self.cursor != nil  || self.data.count == 0 {
            posts.append(Post(uid: "-1", author: "", title: "", body: ""))
        }
        snapshot.appendItems(posts)
        self.dataSource?.apply(snapshot, animatingDifferences: true)
    }

    func applyData(newData: [Post]) {
        var snapshot: NSDiffableDataSourceSnapshot<IntItem, Post>
        if self.data.count == 0 {
            snapshot = NSDiffableDataSourceSnapshot<IntItem, Post>()
            snapshot.appendSections([IntItem(value: 0)])
        } else {
            snapshot = self.dataSource?.snapshot() ?? NSDiffableDataSourceSnapshot<IntItem, Post>()
        }
        var posts = newData
        self.data.append(contentsOf: posts)
        if self.cursor != nil  || self.data.count == 0 {
            posts.append(Post(uid: "-1", author: "", title: "", body: ""))
        }
        snapshot.appendItems(posts)
        self.dataSource?.apply(snapshot, animatingDifferences: true)
    }

    func loadPosts() {
        guard self.isLoading == false else {
            return
        }
        self.isLoading = true
        self.service.getPosts(
            cursor: self.cursor,
            response: { postsResponse, error in
            if let error = error {
                let alert = UIAlertController(
                    title: "Error",
                    message: error.localizedDescription,
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let newData = postsResponse?.posts ?? []
                self.cursor = postsResponse?.paging.nextCursor
                self.applyData(newData: newData)
            }
            self.isLoading = false
        })
    }
}
