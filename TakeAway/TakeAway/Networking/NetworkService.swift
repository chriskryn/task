//
//  NetworkService.swift
//  TakeAway
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import Foundation
protocol NetworkServiceProtocol {
    func getPosts(
        cursor: String?,
        response: @escaping (PostsResponse?, Error?) -> Void
    )
}

class NetworkService {
    private let session: URLSession
    let username: String
    let password: String

    init(username: String, password: String) {
        self.username = username
        self.password = password
        let config = URLSessionConfiguration.default
        let authValue = (username + ":" + password).data(using: .utf8)!.base64EncodedString()
        config.httpAdditionalHeaders = ["Authorization": "Basic \(authValue)"]
        self.session = URLSession(configuration: config)
    }

    func performRequest<T: Decodable> (
        route: Posts,
        completion: @escaping((T?, Error?) -> Void)
    ) {
        guard let url = route.url() else {
            completion(nil, AppError.unknown)
            return
        }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = route.method
        print("URL \(url.absoluteString)")
        let task = self.session.dataTask(with: request) { data, _, error in
            guard let dataResponse = data else {
                DispatchQueue.main.async {
                    completion(nil, AppError.networkError)
                }
                return
            }
            guard error == nil else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: dataResponse)
                DispatchQueue.main.async {
                    completion(decoded, nil)
                }
            } catch {
                print("Error \(error.localizedDescription)")
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        }
        task.resume()
    }

    func getPosts(
        cursor: String? = nil,
        response: @escaping (PostsResponse?, Error?) -> Void
    ) {
        self.performRequest(route: .get(cursor: cursor), completion: response)
    }
}
