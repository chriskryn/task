//
//  EndpointsProtocol.swift
//  TakeAway
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import Foundation

protocol EndpointsProtocol {
    var method: String { get }
    func path() -> String
    func url() -> URL?

}
