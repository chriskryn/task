//
//  Posts.swift
//  TakeAway
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import Foundation

enum Posts: EndpointsProtocol {
    case get(cursor: String?)
    var method: String {
        return "GET"
    }

    func path() -> String {
        switch self {
        case .get(let cursor):
            if let nextCursor = cursor {
                return "\(kBaseUrl)posts/?after=\(nextCursor)"
            }
            return "\(kBaseUrl)posts"
        }
    }

    func url() -> URL? {
        switch self {
        case .get(_):
            return URL(string: self.path())
      }
    }

}
