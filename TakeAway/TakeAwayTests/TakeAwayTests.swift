//
//  TakeAwayTests.swift
//  TakeAwayTests
//
//  Created by Krzysztof Kryniecki on 02/02/2022.
//

import XCTest
@testable import TakeAway

class MockedNetworkService: NetworkServiceProtocol {
    func getPosts(cursor: String?, response: @escaping (PostsResponse?, Error?) -> Void) {
        response(PostsResponse(paging: Paging(nextCursor: "aa"), posts: []), nil)
    }
}

class TakeAwayTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGettingPosts() {
        let service = MockedNetworkService()
        service.getPosts(cursor: "") { response, _ in
            XCTAssert(response?.posts.count == 0)
        }
    }

    func testAPI() {
        let expectation = XCTestExpectation(description: "test api")
        let service = NetworkService(username: "alalal", password: "testtest")
        service.getPosts { posts, error in
            if let error = error {
                print("FAILED \(error.localizedDescription)")
                XCTFail("fail test")
            } else {
                print("SUCCESS \(String(describing: posts?.posts.count) ?? "") \(posts?.paging.nextCursor)")
                expectation.fulfill()
            }

        }
        wait(for: [expectation], timeout: 10.0)
    }
}
